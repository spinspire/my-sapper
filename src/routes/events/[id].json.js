const { wrapper } = require("../../mongo")
const { ObjectID } = require('mongodb')

module.exports = {
	get: wrapper((req, db) => db.collection('events').findOne(
		{ '_id': ObjectID(req.params.id) })
	),
	patch: wrapper((req, db) => {
		delete req.body._id
		console
		return db.collection('events').updateOne(
			{ '_id': ObjectID(req.params.id) },
			{ '$set': req.body })
	}
	),
	del: wrapper((req, db) => db.collection('events').removeOne({ '_id': ObjectID(req.params.id) }))
}