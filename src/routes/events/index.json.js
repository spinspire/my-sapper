const { wrapper } = require("../../mongo")

module.exports = {
  get: wrapper((req, db) => {
    // by default fetch these fields
    const projection = {
      title: 1,
      files: 1,
    }
    // more fields can be requested by "f" query param
    switch (typeof req.query.f) {
      case "string":
        projection[req.query.f] = 1
        break;
      case "object":
        for (let field of req.query.f) projection[field] = 1
        break;
    }

    return db.collection('events').find({}, { projection }).toArray()
  }),
  post: wrapper((req, db) => db.collection('events').insertOne(req.body)),
}