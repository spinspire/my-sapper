import { writable } from "svelte/store"

/*
cart = {
  items: [
    {
      event: <id>,
      section: section_name,
      // seat is only for assigned seating sections
      label: ...
      price: 123.45,
      quantity: 2, // defaults to 1
      amount: 246.90
    },
    {...}
  ],
  amount: 246.90
}
*/
const cartStore = writable({
  items: [],
  quantity: 0,
  amount: 0
})

cartStore.addUnassignedTicket = (event_id, section, quantity) => {
  const amount = section.price * quantity
  const item = {
    event: event_id,
    section: section.name,
    price: section.price,
    quantity,
    amount: section.price * quantity,
    label: `${quantity} seat${quantity > 1 ? 's' : ''}`,
  }
  cartStore.update(cart => {
    const existing = cart.items.find(i => i.section == item.section)
    if (existing) {
      existing.amount += item.amount
      existing.quantity += item.quantity
      existing.label = `${existing.quantity} seats`
    } else {
      cart.items = cart.items.concat(item)
    }
    cart.amount += amount
    cart.quantity += quantity
    return cart
  })
}

cartStore.toggleAssignedTicket = (event_id, section, seat, label) => {
  if (!seat.status) {
  } else if (seat.status == "incart") {
  }
}

cartStore.addAssignedTicket = (event_id, section, row, seat, label) => {
  const amount = seat.price || section.price
  const item = {
    event: event_id,
    label,
    seat: seat.label,
    section: section.name,
    row: row.label,
    amount: amount,
    quantity: 1
  }
  cartStore.update(cart => {
    const existing = cart.items.find(i => i.section == item.section && i.row == item.row && i.seat == item.seat)
    if (existing) {
      // TODO: remove instead
      throw new Error("Adding already existing seat")
    } else {
      cart.items = cart.items.concat(item)
    }
    cart.amount += amount
    cart.quantity += 1
    return cart
  })
}

cartStore.removeIndex = (index) => {
  cartStore.update(cart => {
    const item = cart.items.splice(index, 1)[0]
    cart.amount -= item.amount
    cart.quantity -= item.quantity || 1
    return cart
  })
}

cartStore.removeSeat = (event_id, section, row, seat, label) => {
  cartStore.update(cart => {
    const index = cart.items.findIndex(i => i.section == section.name && i.row == row.label && i.seat == seat.label)
    if (index >= 0) {
      const item = cart.items.splice(index, 1)[0]
      cart.amount -= item.amount
      cart.quantity -= item.quantity || 1
    }
    return cart
  })
}

export default cartStore