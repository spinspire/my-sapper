const mongo = require('mongodb')
const config = require('./config')
const { migrate } = require('./mongo_migrations')

var client, db

async function init() {
    if (!client) {
        client = await mongo.MongoClient.connect(config.mongo.uri, config.mongo.options)
        db = client.db(config.mongo.db)
        await migrate(db)
    }
    return { client, db }
}

async function cleanup() {
    await client.close()
}

function wrapper(query) {
    return async (req, res, next) => {
        const { db } = await init()
        const result = await query(req, db)
        res.end(JSON.stringify(result))
    }
}


module.exports = {
    init, cleanup, wrapper
}