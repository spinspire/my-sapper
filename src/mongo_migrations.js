/*
Rules of migration:
1. Unique name: No two migrations should have the same name.
2. Apend only: New migrations can be added only at the bottom.
3. No delete/rename: Existing migrations cannot be deleted or renamed.
4. Must be IDEMPOTENT: Repeating a migration should be same as running it once.

Migrations can be async: They are not required to be async, but almost always should be.
 */
const migrations = {
    "added 'sections' field to events document": async(db) => {
        await db.collection('events').updateMany({ sections: null }, { $set: { sections: [] } })
    },
    "add 'files' field to events document": async(db) => {
        await db.collection('events').updateMany({ files: null }, { $set: { files: [] } })
    },
}

/*
 * Run migrations. This is called immediately after connecting to db.
 */
async function migrate(db) {
    const coll = db.collection('migrations')
    for (const name in migrations) {
        // look up the migrations in 'migrations' collection
        const doc = await coll.findOne({ name })
            // run migration only if not found in the collection (already executed)
        if (!doc) {
            console.warn(`RUNNING MIGRATION: ${name}`)
            const func = migrations[name]
                // execute migration
            await func(db)
                // save executed in the database
            await coll.insertOne({ name })
        }
    }
}

export default {
    migrate
}