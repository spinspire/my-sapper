
module.exports = {
    mongo: {
        // connect to MONGO_HOST if set, otherwise 'localhost'
        uri: 'mongodb://' + (process.env.MONGO_HOST || 'localhost'),
        db: process.env.MONGO_DB || 'my-sapper',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }
    },
}