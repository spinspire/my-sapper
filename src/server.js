import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import bodyParser from 'body-parser';
import * as sapper from '@sapper/server';
import fileUpload from './file-upload'
import session from 'express-session'

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

const app = polka() // You can also use Express
app.use(session({
	secret: 'secret',
	resave: false,
	saveUninitialized: true,
	cookie: {
		secure: false // set secure true only if you're on HTTPS
	}
}))
// logging
app.use((req, res, next) => {
	console.log(`SID:${req.sessionID}, ${req.method}:${req.path}`)
	next()
})
fileUpload(app, 'uploads') // this has to be BEFORE  sapper and other middlewares
app.use(
	compression({ threshold: 0 }),
	sirv('static', { dev }),
	bodyParser.json(),
	sapper.middleware({
	})
)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});