/*
if (typeof Object.structuredClone == "undefined") {
    // See https://stackoverflow.com/a/10916838 for a better alternative
    // if (typeof navigator == "undefined") {
    Object.prototype.structuredClone = function() {
            return JSON.parse(JSON.stringify(this))
        }
        // } else {
        // Object.prototype.clone = function () {
        // const v8 = require('v8')
        // return v8.deserialize(v8.serialize(this))
        // }
        // }
}
*/

export function moveInArray(arr, origidx, change) {
    const len = arr.length
    if (change == 0 || origidx < 0 || origidx >= len) return arr
    const newidx = origidx + change
    if (newidx < 0 || newidx >= len) return arr
    const newarr = []
    if (change > 0) { // moving down
        // elements before origidx
        if (origidx > 0) newarr.push(...arr.slice(0, origidx))
            // newidx moving into place of origidx
        newarr.push(arr[newidx])
            // intervening elements
        if (change > 1) newarr.push(...arr.slice(origidx, change))
            // origidx moving into place of newidx
        newarr.push(arr[origidx])
            // remaining elements
        if (newidx < len - 1) newarr.push(...arr.slice(newidx + 1))
    } else { // moving up
        // elements before newidx
        if (newidx > 0) newarr.push(...arr.slice(0, newidx))
            // origidx moving into place of newidx
        newarr.push(arr[origidx])
            // intervening elements
        if (-change > 1) newarr.push(...arr.slice(newidx, -change))
            // newidx moving into place of origidx
        newarr.push(arr[newidx])
            // remaining elements
        if (origidx < len - 1) newarr.push(...arr.slice(origidx + 1))
    }
    return newarr
}