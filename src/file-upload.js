import multer from 'multer'
import sirv from 'sirv';
import fs from 'fs'

const dev = process.env.NODE_ENV === 'development';

// app: express or polka instance
// uploadDir: path relative to server-root where files will be uploaded
export default (app, uploadDir = 'uploads') => {
  const upload = multer({
    storage: multer.diskStorage({
      destination: uploadDir,
      filename: function (req, file, cb) {
        /* example file:
        {
          fieldname: 'foo',
          originalname: 'flyer.jpeg',
          encoding: '7bit',
          mimetype: 'image/jpeg',
          destination: 'uploads',
          filename: 'foo-1575686652567-flyer.jpeg',
          path: 'uploads/foo-1575686652567-flyer.jpeg',
          size: 188370
        }
        */
        const subdir = req.params.id || '.'
        const subdirpath = uploadDir + '/' + subdir
        fs.mkdirSync(subdirpath, { recursive: true }) // ensure subdirpath exists
        const path = subdir + '/' + file.fieldname + '-' + Date.now() + '-' + file.originalname
        cb(null, path)
      }
    }),
    limits: {
      fileSize: 10 * 1024 * 1024 // max file size: 10MB
    }
  })

  // upload route ("/uploads") is derived from uploadDir ("upload")
  const uploadRoute = '/' + uploadDir
  app.use(uploadRoute, upload.any()) // middleware for file uploads
  app.post(uploadRoute + '/:id', (req, res) => { // POST handler for file uploads
    const result = { success: true, files: req.files }
    res.end(JSON.stringify(result))
  })
  // DELETE a single file
  app.delete(uploadRoute + '/:id/:file', (req, res) => {
    const filepath = uploadDir + '/' + req.params.id + '/' + req.params.file
    console.log('DELETEing', filepath)
    fs.unlink(filepath, () => {
      res.end(JSON.stringify({ success: true }))
    })
  })
  // DELETE a whole dir (DANGEROUS; remove this)
  app.delete(uploadRoute + '/:id', (req, res) => {
    const subdirpath = uploadDir + '/' + req.params.id
    console.log('DELETEing', subdirpath)
    fs.rmdir(subdirpath, { recursive: true }, () => {
      res.end(JSON.stringify({ success: true }))
    })
  })
  // Handle download of static file uploads. This should be last, otherwise it
  // will interfere with the others above.
  app.use(uploadRoute, sirv(uploadDir, { dev }))
}