# my-sapper

A sample application created using sapper template. It uses sapper and svelte. It then
adds the following:

- ability to and and edit content (posts/events)
- mongodb for persistence
- rich text editor for editing content body fields
- A custom FileUpload component for uploading files and corresponding file-upload.js for handing and saving them on the server.
- and more ...

## Getting started

### Getting the code

Get this code using `git clone` or similar.

### Setup

- mongodb: this project needs to connect to a mongodb server. So your choices are:
  - install mongodb locally (default config assumption)
  - connect to one on the network
  - use ssh port forwarding (e.g. `ssh -L 27017:myserver.inthecloud.com:27017`) to make a mongodb server in cloude available locally
  - use MongoDB Atlas to rent a mongodb server instance in cloud
- edit src/config.js to match your configuration

### Running the project

However you get the code, you can install dependencies and run the project in development mode with:

```bash
cd my-sapper
npm install # or yarn
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.

Consult [sapper.svelte.dev](https://sapper.svelte.dev) for help getting started.